import java.io.*;
import java.util.*;

public class Main {

    public static void getEvenAndOddNumbers(String inputs, String output) throws FileNotFoundException {
        int count = 0;
        int numbers = 0;
        List<Integer> numberList = new ArrayList<>();
        File inp = new File(inputs);
        Scanner in = new Scanner(inp);
        PrintWriter out = new PrintWriter(output);
        while (in.hasNextInt()) {
            numbers = in.nextInt();
            numberList.add(numbers);
        }
        for (int i = 0; i < numberList.size(); i++) {
            count++;
        }
        if (count % 2 != 0) {
            numberList.stream().filter(n -> n % 2 != 0).forEach(out::println);
        } else {
            numberList.stream().filter(n -> n % 2 == 0).forEach(out::println);
        }
        in.close();
        out.close();

        System.out.println("Čísla byla přečtena ze souboru: " + inputs + "," + " výsledek byl zapsán do souboru:" + output);
    }

    public static void main(String[] args) throws FileNotFoundException {
        getEvenAndOddNumbers("C:\\input.txt", "C:\\output.txt");
    }
}






